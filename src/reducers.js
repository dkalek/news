import { combineReducers } from "redux";
import { articlesReducer } from "./modules/Articles/reducer";
import { filtersReducer } from "./modules/Filters/reducer";

const rootReducer = combineReducers({
  articles: articlesReducer,
  filters: filtersReducer
});

export default rootReducer;
