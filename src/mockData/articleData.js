export const articleData = {
  source: { id: null, name: "Lifehacker.com" },
  author: "David Murphy",
  title: "Tell Us Your Most Annoying Tech Problems",
  description: "Each week, we publish a tech-advice column ( Tech 911 )",
  url:
    "https://lifehacker.com/tell-us-your-most-annoying-tech-problems-1837867809",
  urlToImage:
    "https://i.kinja-img.com/gawker-media/image/upload/s--Of6dudBc--/c_fill,fl_progressive,g_center,h_900,q_80,w_1600/qinp1z2tdkkfhrs6cudj.jpg",
  publishedAt: "2019-09-04T14:30:00Z",
  content:
    "Each week, we publish a tech-advice column (Tech 911), where the goal is to take one of your pressing tech questions and provide as many helpful answers as possible."
};
