import React from "react";
import PropTypes from "prop-types";

const Button = ({ text, className, handlerClick }) => (
  <div
    onClick={handlerClick}
    className={className ? `button ${className}` : "button"}
  >
    {text}
  </div>
);

Button.propTypes = {
  text: PropTypes.string.isRequired,
  className: PropTypes.string,
  handlerClick: PropTypes.func
};

export default Button;
