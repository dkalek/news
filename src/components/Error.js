import React from "react";
import PropTypes from "prop-types";

const Error = ({ errorMsg }) => <div className="error-msg">{errorMsg}</div>;

Error.propTypes = {
  errorMsg: PropTypes.string.isRequired
};

export default Error;
