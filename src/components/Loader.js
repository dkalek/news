import React from "react";
import ReactLoading from "react-loading";

const Loader = () => (
  <div className="loader">
    <ReactLoading type={"spin"} color={"#493BE1"} height={40} width={40} />
  </div>
);

export default Loader;
