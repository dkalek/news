import React, { Component } from "react";
import PropTypes from "prop-types";
import arrowDown from "../assets/images/arrow_down.svg";

class Select extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showOptions: false
    };
  }
  toggleOptions = () => {
    this.setState(prevState => ({
      showOptions: !prevState.showOptions
    }));
  };
  handlerClick = (index, option) => {
    const { handlerClick } = this.props;
    this.setState({ showOptions: false });
    handlerClick(index, option);
  };
  render() {
    const { showOptions } = this.state;
    const { placeholder, options, selectedValue, index } = this.props;
    return (
      <div className="select">
        <div
          className={selectedValue ? "label selected" : "label"}
          onClick={this.toggleOptions}
        >
          {selectedValue || placeholder}
          <img src={arrowDown} alt="" />
        </div>
        {showOptions && (
          <ul className="options">
            {options.map(option => (
              <li
                className={selectedValue === option ? "selected" : ""}
                onClick={() => this.handlerClick(index, option)}
                key={option}
              >
                {option}
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  }
}

Select.propTypes = {
  placeholder: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  handlerClick: PropTypes.func
};

export default Select;
