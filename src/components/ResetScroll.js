import React, { Component } from "react";
import { withRouter } from "react-router";

class ResetScroll extends Component {
  componentDidUpdate(prevProps) {
    const { location } = this.props;
    if (location.pathname !== prevProps.location.pathname) {
      if (document.querySelector("html")) {
        document.querySelector("html").scrollTop = 0;
      }
    }
  }
  render() {
    return <div className="container">{this.props.children}</div>;
  }
}

export default withRouter(ResetScroll);
