import React from "react";
import { Link, Route, Router, Switch } from "react-router-dom";
import { createMemoryHistory } from "history";
import { render, fireEvent, cleanup } from "@testing-library/react";
import ResetScroll from "../../components/ResetScroll";

const Page1 = () => <div>You are on Page1</div>;
const Page2 = () => <div>You are on Page2</div>;

const App = () => {
  return (
    <div>
      <Link to="/">Page1</Link>
      <Link to="/page2">Page2</Link>
      <ResetScroll>
        <Switch>
          <Route exact path="/" component={Page1} />
          <Route path="/page2" component={Page2} />
        </Switch>
      </ResetScroll>
    </div>
  );
};

const renderWithRouter = (
  ui,
  {
    route = "/",
    history = createMemoryHistory({ initialEntries: [route] })
  } = {}
) => {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    history
  };
};

afterEach(cleanup);

test("check if scroll is reset", () => {
  const { container, getByText } = renderWithRouter(<App />);
  expect(container.innerHTML).toMatch("You are on Page1");
  document.querySelector("html").scrollTop = 100;
  fireEvent.click(getByText("Page2"));
  expect(container.innerHTML).toMatch("You are on Page2");
  expect(document.querySelector("html").scrollTop).toBe(0);
});
