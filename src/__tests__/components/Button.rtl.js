import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, cleanup } from "@testing-library/react";
import Button from "../../components/Button";

afterEach(cleanup);

test("can render Button with some props", () => {
  const text = "Button test";
  const onClick = jest.fn();
  const { getByText } = render(
    <Button text={text} className="test-css" handlerClick={onClick} />
  );
  const button = getByText(text);
  expect(button).toBeInTheDocument();
  expect(button).toHaveClass("test-css");
  fireEvent.click(button);
  expect(onClick).toHaveBeenCalled();
});
