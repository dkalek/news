import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent, cleanup } from "@testing-library/react";
import Select from "../../components/Select";

afterEach(cleanup);

test("can render Select with some props", () => {
  const placeholder = "Placeholder select";
  const options = ["Tech", "Travel", "Politics", "Sports"];
  const { getByText } = render(
    <Select placeholder={placeholder} options={options} />
  );
  const select = getByText(placeholder);
  expect(select).toBeInTheDocument();
});

test("check show/hide options", () => {
  const placeholder = "Placeholder select";
  const options = ["Tech", "Travel", "Politics", "Sports"];
  const { getByText, queryByText } = render(
    <Select placeholder={placeholder} options={options} />
  );
  const select = getByText(placeholder);
  expect(select).toBeInTheDocument();
  options.map(option => expect(queryByText(option)).not.toBeInTheDocument());
  fireEvent.click(select);
  options.map(option => expect(queryByText(option)).toBeInTheDocument());
  fireEvent.click(select);
  options.map(option => expect(queryByText(option)).not.toBeInTheDocument());
});

test("check if selected value is display instead of placeholder and test handlerClick", () => {
  const placeholder = "Placeholder select";
  const options = ["Tech", "Travel", "Politics", "Sports"];
  const optionText = "Politics";
  const onClick = jest.fn();
  const { getByText, queryByText } = render(
    <Select
      placeholder={placeholder}
      selectedValue="Politics"
      options={options}
      handlerClick={onClick}
    />
  );
  expect(queryByText(placeholder)).not.toBeInTheDocument();
  expect(queryByText(optionText)).toBeInTheDocument();
  const select = getByText(optionText);
  fireEvent.click(select);
  fireEvent.click(queryByText("Sports"));
  expect(onClick).toHaveBeenCalled();
});
