import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import Error from "../../components/Error";

afterEach(cleanup);

test("can render Error with some props", () => {
  const text = "Something went wrong";
  const { getByText } = render(<Error errorMsg={text} />);
  const error = getByText(text);
  expect(error).toBeInTheDocument();
});
