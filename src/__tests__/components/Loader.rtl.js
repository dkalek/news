import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import Loader from "../../components/Loader";

afterEach(cleanup);

test("can render Loader with some props", () => {
  const { container } = render(<Loader />);
  const loader = container.querySelector(".loader");
  expect(loader).toBeInTheDocument();
});
