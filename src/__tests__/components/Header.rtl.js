import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import Header from "../../components/Header";

afterEach(cleanup);

test("can render Header with some props", () => {
  const text = "Title";
  const { getByText } = render(<Header title={text} />);
  const header = getByText(text);
  expect(header).toBeInTheDocument();
});
