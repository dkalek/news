import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import {
  render,
  cleanup,
  waitForElement,
  fireEvent
} from "@testing-library/react";
import axiosMock from "axios";
import { articlesReducer } from "../../../modules/Articles/reducer";
import { filtersReducer } from "../../../modules/Filters/reducer";
import Articles from "../../../pages/Articles";
import { articleData } from "../../../mockData/articleData";

const rootReducer = combineReducers({
  articles: articlesReducer,
  filters: filtersReducer
});

const renderWithRedux = (
  ui,
  { initialState, store = createStore(rootReducer, initialState) } = {}
) => {
  return {
    ...render(<Provider store={store}>{ui}</Provider>),
    store
  };
};

afterEach(cleanup);

test("can render Articles page without data", async () => {
  axiosMock.get.mockResolvedValueOnce({
    data: { articles: [], totalResults: 0 }
  });
  const { container } = renderWithRedux(<Articles />);
  const loader = container.querySelector(".loader");
  expect(loader).toBeInTheDocument();
});

test("can render Articles page with one article and hidden Show more", async () => {
  axiosMock.get.mockResolvedValueOnce({
    data: { articles: [articleData], totalResults: 1 }
  });
  const { getByText, queryByText } = renderWithRedux(
    <Router>
      <Articles />
    </Router>
  );
  const titleHeader = await waitForElement(() => getByText("Articles"));
  expect(titleHeader).toBeInTheDocument();
  expect(queryByText("Show More")).not.toBeInTheDocument();
});

test("can render Articles with Show more", async () => {
  axiosMock.get.mockResolvedValueOnce({
    data: { articles: [articleData], totalResults: 2 }
  });
  const { getByText, queryByText } = renderWithRedux(
    <Router>
      <Articles />
    </Router>
  );
  const titleHeader = await waitForElement(() => getByText("Articles"));
  expect(titleHeader).toBeInTheDocument();
  const showMore = queryByText("Show More");
  expect(showMore).toBeInTheDocument();
  axiosMock.get.mockResolvedValueOnce({
    data: { articles: [{ title: "Test title " }], totalResults: 2 }
  });
  fireEvent.click(showMore);
  expect(queryByText("Show More")).not.toBeInTheDocument();
});
