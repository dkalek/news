import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import ArticleBox from "../../../pages/Articles/ArticleBox";
import { articleData } from "../../../mockData/articleData";

afterEach(cleanup);

test("can render ArticleBox with some props", () => {
  const { container, getByText } = render(
    <Router>
      <ArticleBox data={articleData} />
    </Router>
  );
  const image = container.querySelector("img");
  expect(image).toBeInTheDocument();
  const date = getByText("September 4, 2019");
  expect(date).toBeInTheDocument();
  const author = getByText(articleData.author);
  expect(author).toBeInTheDocument();
  const sourceName = getByText(articleData.source.name);
  expect(sourceName).toBeInTheDocument();
  const title = getByText(articleData.title);
  expect(title).toBeInTheDocument();
  const description = getByText(articleData.description);
  expect(description).toBeInTheDocument();
  const linkMore = getByText("Read more");
  expect(linkMore).toBeInTheDocument();
});

test("can render placeholder for image and test empty data", () => {
  const { getByText, queryByText } = render(
    <Router>
      <ArticleBox data={{}} />
    </Router>
  );
  const placeholder = getByText("No image");
  expect(placeholder).toBeInTheDocument();
  const date = queryByText("September 4, 2019");
  expect(date).not.toBeInTheDocument();
  const author = queryByText(articleData.author);
  expect(author).not.toBeInTheDocument();
  const sourceName = queryByText(articleData.source.name);
  expect(sourceName).not.toBeInTheDocument();
  const title = queryByText(articleData.title);
  expect(title).not.toBeInTheDocument();
  const description = queryByText(articleData.description);
  expect(description).not.toBeInTheDocument();
  const linkMore = queryByText("Read more");
  expect(linkMore).not.toBeInTheDocument();
});
