import React from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import { initialState, filtersReducer } from "../../../modules/Filters/reducer";
import Filters from "../../../pages/Articles/Filters";

const store = createStore(filtersReducer, { filters: initialState });

afterEach(cleanup);

test("can render filters with store", () => {
  const { getByText } = render(
    <Provider store={store}>
      <Filters getArticles={jest.fn()} />
    </Provider>
  );
  const filterTopic = getByText("Tech");
  expect(filterTopic).toBeInTheDocument();
  const filterSort = getByText("Sort by");
  expect(filterSort).toBeInTheDocument();
  const filterDates = getByText("Dates");
  expect(filterDates).toBeInTheDocument();
});
