import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "@testing-library/jest-dom/extend-expect";
import { render, cleanup } from "@testing-library/react";
import Article from "../../../pages/Article";
import { articleData } from "../../../mockData/articleData";

const history = {
  location: {
    state: {
      article: articleData
    }
  }
};

afterEach(cleanup);

test("can render Article page with data", () => {
  const { container, getByText } = render(
    <Router>
      <Article history={history} />
    </Router>
  );
  const header = container.querySelector(".header");
  expect(header).toBeInTheDocument();
  const linkBack = getByText("Return to articles list");
  expect(linkBack).toBeInTheDocument();
  const image = container.querySelector("img");
  expect(image).toBeInTheDocument();
  const date = getByText("September 4, 2019");
  expect(date).toBeInTheDocument();
  const author = getByText(articleData.author);
  expect(author).toBeInTheDocument();
  const sourceName = getByText(articleData.source.name);
  expect(sourceName).toBeInTheDocument();
  const title = container.querySelector(".article__title");
  expect(title).toBeInTheDocument();
  const content = getByText(articleData.content);
  expect(content).toBeInTheDocument();
  const linkToSource = getByText("Go To Source");
  expect(linkToSource).toBeInTheDocument();
});
