import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import ResetScroll from "./components/ResetScroll";
import Articles from "./pages/Articles";
import Article from "./pages/Article";
import NoMatch from "./pages/NoMatch";
import reducers from "./reducers";
import "./styles/style.scss";

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <ResetScroll>
          <Switch>
            <Route exact path="/" component={Articles} />
            <Route exact path="/article/:title" component={Article} />
            <Route component={NoMatch} />
          </Switch>
        </ResetScroll>
      </Router>
    </Provider>
  );
};

export default App;
