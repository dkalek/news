import { UPDATE_FILTERS, CLEAR_FILTERS } from "./constants";

export const dispatchUpdateFilters = filters => ({
  type: UPDATE_FILTERS,
  filters
});

export const dispatchClearFilters = () => ({
  type: CLEAR_FILTERS
});
