import produce from "immer";
import { UPDATE_FILTERS, CLEAR_FILTERS } from "./constants";

export const initialState = {
  filters: [
    {
      label: "Topic",
      options: ["Tech", "Travel", "Politics", "Sports"],
      selectedValue: "Tech"
    },
    {
      label: "Sort by",
      options: ["Popularity", "PublishedAt"],
      selectedValue: null
    },
    {
      label: "Dates",
      options: ["This month", "This week", "Today"],
      selectedValue: null
    }
  ]
};

export function filtersReducer(state = initialState, action) {
  return produce(state, draft => {
    switch (action.type) {
      case UPDATE_FILTERS:
        draft.filters = action.filters;
        break;
      case CLEAR_FILTERS:
        const filters = initialState.filters;
        filters[0].selectedValue = "Tech";
        filters[1].selectedValue = null;
        filters[2].selectedValue = null;
        draft.filters = filters;
        break;
      default:
        return;
    }
  });
}
