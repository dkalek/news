import { createSelector } from "reselect";
import { REDUCER_KEY } from "./constants";

const getFilters = state => state[REDUCER_KEY].filters;

export const getFiltersSelector = createSelector(
  getFilters,
  filters => filters
);
