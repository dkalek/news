import produce from "immer";
import { UPDATE_ARTICLES } from "./constants";

const initialState = {
  articles: [],
  page: 1,
  totalResults: null
};

export function articlesReducer(state = initialState, action) {
  return produce(state, draft => {
    switch (action.type) {
      case UPDATE_ARTICLES:
        draft.articles = action.articles;
        draft.page = action.page;
        draft.totalResults = action.totalResults;
        break;
      default:
        return;
    }
  });
}
