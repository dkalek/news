import { UPDATE_ARTICLES } from "./constants";

export const dispatchUpdateArticles = (articles, page, totalResults) => ({
  type: UPDATE_ARTICLES,
  articles,
  page,
  totalResults
});
