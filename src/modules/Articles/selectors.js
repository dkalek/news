import { createSelector } from "reselect";
import { REDUCER_KEY } from "./constants";

const getArticles = state => state[REDUCER_KEY].articles;
const getPage = state => state[REDUCER_KEY].page;
const getTotalResults = state => state[REDUCER_KEY].totalResults;

export const getArticlesSelector = createSelector(
  getArticles,
  articles => articles
);

export const getPageSelector = createSelector(
  getPage,
  page => page
);

export const getTotalResultsSelector = createSelector(
  getTotalResults,
  totalResults => totalResults
);
