import React, { Component } from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import Error from "../../components/Error";
import Header from "../../components/Header";

class Article extends Component {
  render() {
    const { history } = this.props;
    const state = history.location.state;
    const article = state && state.article;
    if (article) {
      return (
        <div className="article">
          <Header title={article.title} />
          <Link to="/">Return to articles list</Link>
          <div className="article__image">
            {article.urlToImage && <img src={article.urlToImage} alt="" />}
            {!article.urlToImage && <div className="placeholder">No image</div>}
          </div>
          <div className="article__content">
            <div className="article__info">
              <span>{moment(article.publishedAt).format("LL")}</span>
              <span>{article.author}</span>
              {article.url && (
                <a href={article.url} target="_blank" rel="noopener noreferrer">
                  {article.source && article.source.name}
                </a>
              )}
            </div>
            <div className="article__title">{article.title}</div>
            <div className="article__description">
              {article.content ? article.content : article.description}
            </div>
            {article.url && (
              <a
                className="button-source"
                href={article.url}
                target="_blank"
                rel="noopener noreferrer"
              >
                Go To Source
              </a>
            )}
          </div>
        </div>
      );
    }
    return <Error errorMsg="No data" />;
  }
}

export default Article;
