import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Select from "../../components/Select";
import Button from "../../components/Button";
import {
  dispatchUpdateFilters,
  dispatchClearFilters
} from "../../modules/Filters/actions";
import { getFiltersSelector } from "../../modules/Filters/selectors";

const DEFAULT_TOPIC = "Tech";

class Filters extends Component {
  setFilter = (index, option) => {
    const { filters, setFilters, getArticles } = this.props;
    const selectedValue = filters[index].selectedValue;
    if (index === 0) {
      if (selectedValue === option) {
        filters[index].selectedValue = DEFAULT_TOPIC;
      } else {
        filters[index].selectedValue = option;
      }
    } else {
      if (selectedValue === option) {
        filters[index].selectedValue = null;
      } else {
        filters[index].selectedValue = option;
      }
    }
    setFilters(Object.assign([], filters));
    const topic = filters[0].selectedValue;
    const sortBy = filters[1].selectedValue;
    const dates = filters[2].selectedValue;
    const page = 1;
    getArticles(topic, page, sortBy, dates, true);
  };
  clearFilters = () => {
    const { clearFilters, getArticles } = this.props;
    clearFilters();
    getArticles(DEFAULT_TOPIC, 1, null, null, true);
  };
  render() {
    const { filters } = this.props;
    return (
      <div className="filters">
        <div className="filters__left">
          {filters.map((filter, index) => (
            <Select
              key={filter.label}
              placeholder={filter.label}
              options={filter.options}
              selectedValue={filter.selectedValue}
              index={index}
              handlerClick={this.setFilter}
            />
          ))}
        </div>
        <div className="filters__right">
          <Button text="Clear Filters" handlerClick={this.clearFilters} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  filters: getFiltersSelector(state)
});

const mapDispatchToProps = dispatch => ({
  setFilters: filters => {
    dispatch(dispatchUpdateFilters(filters));
  },
  clearFilters: () => {
    dispatch(dispatchClearFilters());
  }
});

Filters.propTypes = {
  filters: PropTypes.array.isRequired,
  getArticles: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filters);
