import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import moment from "moment";
import Loader from "../../components/Loader";
import Error from "../../components/Error";
import Header from "../../components/Header";
import Button from "../../components/Button";
import ArticleBox from "./ArticleBox";
import Filters from "./Filters";
import {
  getArticlesSelector,
  getPageSelector,
  getTotalResultsSelector
} from "../../modules/Articles/selectors";
import { dispatchUpdateArticles } from "../../modules/Articles/actions";
import { getFiltersSelector } from "../../modules/Filters/selectors";
import config from "../../config";

const PAGE_SIZE = 6;
const DEFAULT_TOPIC = "Tech";
const FILTER_MONTH = "This month";
const FILTER_WEEK = "This week";
const FILTER_TODAY = "Today";
const START_OF_MONTH = moment()
  .startOf("month")
  .format("YYYY-MM-DD");
const END_OF_MONTH = moment()
  .endOf("month")
  .format("YYYY-MM-DD");
const START_OF_WEEK = moment()
  .startOf("week")
  .format("YYYY-MM-DD");
const END_OF_WEEK = moment()
  .endOf("week")
  .format("YYYY-MM-DD");
const TODAY = moment().format("YYYY-MM-DD");

class Articles extends Component {
  state = {
    loading: false,
    loadingNewArticles: false,
    error: null,
    topic: DEFAULT_TOPIC,
    showMore: true
  };
  componentDidMount() {
    const { topic } = this.state;
    const { articles, page } = this.props;
    if (!articles.length) {
      this.getArticles(topic, page);
    }
  }
  getArticles = async (
    q,
    page,
    sortBy = false,
    dates = false,
    setNewArticles = false
  ) => {
    const { articles, setArticles } = this.props;
    let articlesUrl = `${config.API_NEWS}/everything?q=${q}&apiKey=${config.API_KEY}&pageSize=${PAGE_SIZE}&page=${page}`;
    if (sortBy) {
      articlesUrl += `&sortBy=${sortBy}`;
    }
    if (dates) {
      if (dates === FILTER_MONTH) {
        articlesUrl += `&from=${START_OF_MONTH}&to=${END_OF_MONTH}`;
      }
      if (dates === FILTER_WEEK) {
        articlesUrl += `&from=${START_OF_WEEK}&to=${END_OF_WEEK}`;
      }
      if (dates === FILTER_TODAY) {
        articlesUrl += `&from=${TODAY}&to=${TODAY}`;
      }
    }
    if (setNewArticles) {
      this.setState({ loadingNewArticles: true });
    } else {
      this.setState({ loading: true });
    }
    try {
      const resp = await axios.get(articlesUrl);
      let newArticles = [];
      if (setNewArticles) {
        newArticles = resp.data.articles;
      } else {
        newArticles = articles.concat(resp.data.articles);
      }
      setArticles(newArticles, page, resp.data.totalResults);
      this.setState({
        loading: false,
        loadingNewArticles: false,
        showMore: newArticles.length !== resp.data.totalResults
      });
    } catch (error) {
      this.setState({
        loading: false,
        loadingNewArticles: false,
        error: "Something went wrong"
      });
    }
  };
  loadMore = () => {
    const { page, filters } = this.props;
    const topic = filters[0].selectedValue;
    const sortBy = filters[1].selectedValue;
    const dates = filters[2].selectedValue;
    this.getArticles(topic, page + 1, sortBy, dates, false);
  };
  render() {
    const { loading, loadingNewArticles, error, showMore } = this.state;
    const { articles } = this.props;
    if ((loading && !articles.length) || loadingNewArticles) {
      return <Loader />;
    }
    if (error) {
      return <Error errorMsg={error} />;
    }
    return (
      <div className="articles">
        <Header title="Articles" />
        <Filters getArticles={this.getArticles} />
        <div className="articles-list">
          {articles.map((article, index) => (
            <ArticleBox key={`${article.title}_${index}`} data={article} />
          ))}
        </div>
        {showMore && (
          <div className="load-more-bar">
            {!loading && (
              <Button
                text="Show More"
                className="load-more"
                handlerClick={this.loadMore}
              />
            )}
            {loading && articles.length && <Loader />}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  articles: getArticlesSelector(state),
  page: getPageSelector(state),
  totalResults: getTotalResultsSelector(state),
  filters: getFiltersSelector(state)
});

const mapDispatchToProps = dispatch => ({
  setArticles: (articles, page, totalResults) => {
    dispatch(dispatchUpdateArticles(articles, page, totalResults));
  }
});

Articles.propTypes = {
  articles: PropTypes.array.isRequired,
  page: PropTypes.number.isRequired,
  totalResults: PropTypes.number,
  filters: PropTypes.array.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Articles);
