import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import moment from "moment";

class ArticleBox extends Component {
  render() {
    const { data } = this.props;
    const title = data.title && data.title.replace(/ /g, "-");
    return (
      <div className="article-box">
        {data.urlToImage && <img src={data.urlToImage} alt="" />}
        {!data.urlToImage && <div className="placeholder">No image</div>}
        <div className="article-box__info">
          <span>{moment(data.publishedAt).format("LL")}</span>
          <span>{data.author}</span>
          {data.url && (
            <a href={data.url} target="_blank" rel="noopener noreferrer">
              {data.source && data.source.name}
            </a>
          )}
        </div>
        <div className="article-box__title">{data.title}</div>
        <div className="article-box__description">{data.description}</div>
        {title && (
          <Link
            className="button button-more"
            to={{
              pathname: `/article/${title}`,
              state: { article: data }
            }}
          >
            Read more
          </Link>
        )}
      </div>
    );
  }
}

ArticleBox.propTypes = {
  data: PropTypes.object.isRequired
};

export default ArticleBox;
