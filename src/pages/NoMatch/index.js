import React, { Component } from "react";

class NoMatch extends Component {
  render() {
    return <div className="not-found">NoMatch</div>;
  }
}

export default NoMatch;
